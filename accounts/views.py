from django.contrib.auth import (login,logout,authenticate,get_user_model)
from django.shortcuts import render
from .forms import Userloginform

# Create your views here.
def login_view(request):
    form=Userloginform(request.POST or None)
    if form.is_valid():
        username=form.cleaned_data.get('username')
        password=form.cleaned_data.get('password')
        user=authenticate(username="username",password="password")
        login(user)
    context = {
        'form':form,
        'title':'Login'
    }
    return render(request,'accounts/forms.html',context)

def logout_view(request):
    return render(request,'accounts/forms.html',context={})


def register_view(request):
    return render(request,'accounts/forms.html',context={})