from django import forms
from django.contrib.auth.models import User

class Userloginform(forms.ModelForm):
    class Meta:
        model = User
        fields = ['username','password']