from django.core.paginator import Paginator
from django.shortcuts import render,get_object_or_404,redirect
from django.http import HttpResponse,HttpResponseRedirect
from .forms import Postform
from .models import Post
from django.contrib import messages



def post_create(request):
    form = Postform()
    if request.method == "POST":
        form= Postform(request.POST,request.FILES or None)
        if form.is_valid():
            instance=form.save(commit=False)
            instance.save()
            messages.success(request,"Post Created Succesfully..!!")
            return HttpResponseRedirect(instance.get_absolute_url())
        messages.error(request,'Some error occured..!!!')
    context={
        "form":form,
        "title": "Create Post",
        "button": "Create Post"
    }
    return render(request,'posts/post_form.html',context)

def post_delete(request,id):
    queryset= get_object_or_404(Post,id=id)
    queryset.delete()
    messages.success(request,"Post deleted Successfully...")
    return redirect('posts:lists')


def post_detail(request,id):
    queryset = get_object_or_404(Post,id=id)
    context={
        "title":"Detail",
        "object" : queryset
        }
    return render(request,'posts/post_detail.html',context)

def post_list(request):
    queryset = Post.objects.all()
    paginator = Paginator(queryset, 5)  # Show 25 contacts per page

    page = request.GET.get('page')
    query = paginator.get_page(page)
    context={
        'title': "List",
        'object_list': queryset,
        'contacts': query
        }
    return render(request,'posts/post_list.html', context)
    # return HttpResponse("<h1> hello world! list</h1>")

    
def post_update(request,id):
    instance = get_object_or_404(Post,id=id)
    form=Postform(request.POST or None,request.FILES or None,instance=instance)
    if request.method == "POST":    
        if form.is_valid():
            obj=form.save(commit= False)
            obj.save()
            messages.success(request,"Post Updated...!!")
            return HttpResponseRedirect(obj.get_absolute_url())
        
    context={
        "form":form,
        "title": "Update Blog",
        "button":"Update Post" 
        }            
    return render(request,'posts/post_form.html',context)    
    
