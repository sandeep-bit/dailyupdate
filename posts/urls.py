from django.urls import path
from . import views

urlpatterns = [
    path('', views.post_list, name="lists"),
    path('<int:id>/', views.post_detail,name="detail"),
    path('create/', views.post_create,name="create"),
    path('<int:id>/delete/', views.post_delete,name="delete"),
    path('<int:id>/edit/', views.post_update,name="update"),
    
 
]