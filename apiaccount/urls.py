from django.urls import path
from . import views

urlpatterns = [
    path('users/', views.UserCreate.as_view(), name='account-create'),
    path('new/', views.userList.as_view(), name='new'),
    path('lead/', views.LeadListCreate.as_view(), name='lead'),
    path('detail/<int:pk>/', views.UserDetail.as_view(), name='account-detail'),

]